#!/bin/bash
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
cd "/media/Autobleem/Apps/doom"
chmod +x "crispy-doom"
echo -n 2 > "/data/power/disable"
HOME="/media/Autobleem/Apps/doom" ./crispy-doom > "${RUNTIME_LOG_PATH}/ab_crispydoom.log" 2>&1
echo -n 1 > "/data/power/disable"
cd "/var/volatile/launchtmp"
echo "launch_StockUI" > "/tmp/launchfilecommand"