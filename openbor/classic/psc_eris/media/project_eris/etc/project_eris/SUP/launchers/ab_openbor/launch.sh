#!/bin/bash
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
cd "/media/Autobleem/Apps/openbor/OpenBOR"
chmod +x "./OpenBOR-psc"
# No idea why STDOUT & STDERR are pointed to dev null...
# I guess this binary needs redoing, the shared libs need static compiling too.
# YOLO
echo -n 2 > "/data/power/disable"
HOME="/media/Autobleem/Apps/openbor" LD_LIBRARY_PATH="./libs:${LD_LIBRARY_PATH}" ./OpenBOR-psc &> "${RUNTIME_LOG_PATH}/openbor.log" #1>/dev/null 2>/dev/null
echo -n 1 > "/data/power/disable"
cd "/var/volatile/launchtmp"
echo "launch_StockUI" > "/tmp/launchfilecommand"