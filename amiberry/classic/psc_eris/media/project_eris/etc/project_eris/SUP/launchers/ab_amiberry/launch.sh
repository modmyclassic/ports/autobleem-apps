#!/bin/bash
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
cd "/media/Autobleem/Apps/amiberry"
cp -f "conf/default/"* "conf/"
chmod +x "amiberry"
echo -n 2 > "/data/power/disable"
HOME="/media/Autobleem/Apps/amiberry" ./amiberry  -config="./conf/AB-A500.uae" > "${RUNTIME_LOG_PATH}/ab_amiberry.log" 2>&1
echo -n 1 > "/data/power/disable"
cd "/var/volatile/launchtmp"
echo "launch_StockUI" > "/tmp/launchfilecommand"